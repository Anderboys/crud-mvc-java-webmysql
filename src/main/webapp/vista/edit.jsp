<%@ page import="ModelDAO.ClienteDAO" %>
<%@ page import="Model.Cliente" %><%--
  Created by IntelliJ IDEA.
  User: Anderboys
  Date: 05/02/2020
  Time: 19:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>

    <%--   this is for DatePicker--%>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            // datetimepicker
            // datepicker
            $( "#datepicker" ).datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth: true,
                changeYear: true
            });
        } );
    </script>
    <%--------  DATEPICKER -------------%>

    <title>JSP Page</title>
</head>
<body>
<div class="container">

    <div class="col-lg-6">

        <%
            ClienteDAO dao=new ClienteDAO();
            int id=Integer.parseInt((String)request.getAttribute("idper"));
            Cliente c=(Cliente) dao.list(id);

        %>

        <h1>Modificar Cliente</h1>
        <form action="Controlador">


            Nombres: <br>
            <input class="form-control" type="text" name="txtNom" value="<%= c.getNombre()%>"><br>
            Apellido:<br>
            <input class="form-control" type="text" name="txtApe"value="<%= c.getApellido()%>"><br>
            Email: <br>
            <input class="form-control" type="text" name="txtEmail" value="<%= c.getEmail()%>"><br>
            Fecha: <br>
            <input class="form-control" type="text" id="datepicker" name="txtFecha" value="<%= c.getCreateAt()%>"><br>

            <input type="hidden" name="txtid" value="<%= c.getId()%>">
            <input class="btn btn-primary" type="submit" name="accion" value="Actualizar">
            <a href="Controlador?accion=listar">Regresar</a>
        </form>
    </div>

</div>
</body>
</html>
