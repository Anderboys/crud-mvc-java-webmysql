<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="ModelDAO.ClienteDAO" %>
<%@ page import="Model.Cliente" %><%--
  Created by IntelliJ IDEA.
  User: Anderboys
  Date: 05/02/2020
  Time: 19:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Listar</title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="container">
    <h1>Personas</h1>
    <a class="btn btn-success" href="Controlador?accion=add">Agregar Nuevo</a>
    <a class="btn btn-success" href="Controlador?accion=prueba">Prueba Datepiker</a>
    <br>
    <br>

<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRES</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">FECHA</th>
    </tr>
    </thead>
    <%
        ClienteDAO dao=new ClienteDAO();
        List<Cliente> list=dao.listar();
        Iterator<Cliente> iter=list.iterator();
        Cliente cli=null;
        while(iter.hasNext()){
            cli=iter.next();

            %>
            <tbody>

            <tr>
                <td class="text-center"><%= cli.getId()%></td>
                <td class="text-center"><%= cli.getNombre()%></td>
                <td class="text-center"><%= cli.getApellido()%></td>
                <td class="text-center"><%= cli.getEmail()%></td>
                <td class="text-center"><%= cli.getCreateAt()%></td>

                <
                <td class="text-center">
                    <a class="btn btn-warning" href="Controlador?accion=editar&id=<%= cli.getId()%>">Editar</a>
                    <a class="btn btn-danger" href="Controlador?accion=eliminar&id=<%= cli.getId()%>">Remove</a>
                </td>
            </tr>
            <%}%>

    </tbody>
</table>
</div>

</body>
</html>
