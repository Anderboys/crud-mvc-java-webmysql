package ModelDAO;
import Config.Conexion;
import Interfaces.CRUD;
import Model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ClienteDAO implements CRUD {

        Conexion cnx = new Conexion();
        Connection con;
        PreparedStatement ps;
        ResultSet rs;
        Cliente cli = new Cliente();

         @Override
        public List listar() {

        ArrayList<Cliente> listaCliente = new ArrayList<>();
        String sql="select * from clientes";

        try {
            con= cnx.getConnection();
            ps=con.prepareStatement(sql);
            rs = ps.executeQuery();

            while(rs.next()){

                Cliente cli = new Cliente();
                cli.setId(rs.getLong("id"));
                cli.setNombre(rs.getString("nombre"));
                cli.setApellido(rs.getString("apellido"));
                cli.setEmail(rs.getString("email"));
                cli.setCreateAt(rs.getDate("create_at"));
                listaCliente.add(cli);
            }

        }catch (Exception e){
        }

        return listaCliente;
    }

    public Cliente list(long id) {

        String sql="select * from clientes where Id="+id;
        try {
            con=cnx.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){
                cli.setId(rs.getLong("Id"));
                cli.setNombre(rs.getString("nombre"));
                cli.setApellido(rs.getString("apellido"));
                cli.setEmail(rs.getString("email"));
                cli.setCreateAt(rs.getDate("create_at"));

            }
        } catch (Exception e) {
        }
        return cli;
    }

    @Override
    public boolean add(Cliente cli)  {

        String sql="insert into clientes(nombre, apellido,email,create_at)values('"+cli.getNombre()+"','"+cli.getApellido()+"','"+cli.getEmail()+"','"+cli.getCreateAt()+"')";
        System.out.println("Query insert");
        System.out.println(sql);

        try {
            con=cnx.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public boolean edit(Cliente cli) {
        String sql="update clientes set nombre='"+cli.getNombre()+"',apellido='"+cli.getApellido()+"',email='"+cli.getEmail()+"',create_at='"+cli.getCreateAt()+"' where Id="+cli.getId();
        try {
            con=cnx.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public boolean eliminar(long id) {
        String sql="delete from clientes where Id="+id;
        try {
            con=cnx.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
}
