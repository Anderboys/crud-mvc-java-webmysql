package Interfaces;

import Model.Cliente;
import Model.Cliente;

import java.text.ParseException;
import java.util.List;

public interface CRUD {

    public List listar();
    public Cliente list(long id);
    public boolean add(Cliente cli) throws ParseException;
    public boolean edit(Cliente cli);
    public boolean eliminar(long id);

}
