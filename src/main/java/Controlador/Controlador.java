package Controlador;

import Model.Cliente;
import ModelDAO.ClienteDAO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class Controlador extends HttpServlet {

    String listar="vista/listar.jsp";
    String add="/vista/add.jsp";
    String edit="/vista/edit.jsp";
    String prueba="/vista/prueba.jsp";
    Cliente c=new Cliente();
    ClienteDAO dao=new ClienteDAO();
    Long id;
    Date date1;
    java.sql.Date date2;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String acceso="";
        String action=request.getParameter("accion");

        if(action.equalsIgnoreCase("listar")){
            acceso=listar;

        }  else if(action.equalsIgnoreCase("add")){
            acceso=add;

        }  else if(action.equalsIgnoreCase("prueba")){
            acceso=prueba;

        } else if(action.equalsIgnoreCase("Agregar")){

            String nom=request.getParameter("txtNom");
            String ape=request.getParameter("txtApe");
            String email=request.getParameter("txtEmail");
            //PARSEAR DE STRING A DATE   -->> CORREGIRRR <<--
            String fecha =  request.getParameter("txtFecha");
//                date1=new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            try {
                date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                date2 = new java.sql.Date(date1.getTime());

//                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz yyyy", Locale.US);
//                dateFormat.parse(fecha);
//                System.out.println(dateFormat.format(new Date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Controlador Agregar");
            System.out.println("nom : " + nom+" ape: "+ape+" email: "+email+" fecha: "+ date2);
            // corregirrrrrrrrrr
            c.setNombre(nom);
            c.setApellido(ape);
            c.setEmail(email);
            c.setCreateAt(date2);
            dao.add(c);
            acceso=listar;

        } else if(action.equalsIgnoreCase("editar")){
            request.setAttribute("idper",request.getParameter("id"));
            acceso=edit;

        }  else if(action.equalsIgnoreCase("Actualizar")){

            id=  Long.parseLong(request.getParameter("txtid"));
            String nom=request.getParameter("txtNom");
            String ape=request.getParameter("txtApe");
            String email=request.getParameter("txtEmail");

            //PARSEAR DE STRING A DATE
            String fecha =  request.getParameter("txtFecha");
            try {
                date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                date2 = new java.sql.Date(date1.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            c.setId( id);
            c.setNombre(nom);
            c.setApellido(ape);
            c.setEmail(email);
            c.setCreateAt(date2);

            dao.edit(c);
            acceso=listar;
        }
        else if(action.equalsIgnoreCase("eliminar")){

            id=  Long.parseLong(request.getParameter("id"));
            c.setId(id);
            dao.eliminar(id);
            acceso=listar;
        }

        
        RequestDispatcher vista=request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }
}
