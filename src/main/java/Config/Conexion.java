package Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexion {

//    Connection con;
//    public Conexion(){
////        com.mysql.jdbc.Driver
//        try {
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/springbootbackend?serverTimezone=UTC","root","");
//        } catch (Exception e) {
//            System.err.println("Fatality Error GG "+e);
//
//        }
//    }
    private static Connection con;
    private static final String driver="com.mysql.cj.jdbc.Driver";
    private static final String user="root";
    private static final String password="";
    private static final String url="jdbc:mysql://localhost:3306/springbootbackend?serverTimezone=UTC";
    public Conexion(){
        con = null;
        try {
            Class.forName(driver);
            con= DriverManager.getConnection(url,user,password);
            if(con!=null){
                System.out.println("Conexion establecida");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Fatality Error Conexion GG "+e);

        }
    }

    public Connection getConnection(){
        return con;
    }
}
